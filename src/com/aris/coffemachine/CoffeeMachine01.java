package com.aris.coffemachine;

import java.util.Scanner;

public class CoffeeMachine01 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int waterNow = 400;
        int milkNow = 540;
        int coffeeBeansNow = 120;
        int disposableCupsNow = 9;
        int moneyNow = 550;

        //ESPRESSO
        int espressoWater = 250;
        int espressoCoffeeBeans = 16;
        int espressoMoney = 4;

        //LATTE
        int latteWater = 350;
        int latteCoffeeBeans = 20;
        int latteMilk = 75;
        int latteMoney = 7;

        //CAPPUCCINO
        int cappuccinoWater = 200;
        int cappuccinoMilk = 100;
        int cappuccinoCoffeeBeans = 12;
        int cappuccinoMoney = 6;

        String action;

        do {
            System.out.print("\nWrite action (buy, fill, take, remaining, exit): ");
            action = sc.next();
            switch (action) {
                case "buy":

                    int espresso = 1;
                    String espressoNum = Integer.toString(espresso);
                    int latte = 2;
                    String latteNum = Integer.toString(latte);
                    int cappuccino = 3;
                    String cappuccinoNum = Integer.toString(cappuccino);
                    var bck = "back";
                    System.out.print("\nWhat do you want to buy? " + espressoNum + " - espresso, " + latteNum + " - latte, " + cappuccinoNum + " - cappuccino, " + bck + " - to main menu: ");
                    String orderCoffee = sc.next();

                    if (orderCoffee.equals(espressoNum)) {


                        if (waterNow < espressoWater) {
                            System.out.println("Sorry, not enough water!");
                        } else if (coffeeBeansNow < espressoCoffeeBeans) {
                            System.out.println("Sorry, not enough Coffee Beans!");
                        } else if (moneyNow <= 0) {
                            System.out.println("Sorry, not enough money for change!");
                        } else if (disposableCupsNow <= 0) {
                            System.out.println("Sorry, not enough disposable cups!");
                        } else {
                            System.out.println("I have enough resources, making you a coffee!");
                        }

                        if (waterNow > espressoWater && coffeeBeansNow > espressoCoffeeBeans) {
                            waterNow -= espressoWater;
                            coffeeBeansNow -= espressoCoffeeBeans;
                            moneyNow += espressoMoney;
                            disposableCupsNow -= 1;
                        }

                    } else if (orderCoffee.equals(latteNum)) {


                        if (waterNow < latteWater) {
                            System.out.println("Sorry, not enough water!");
                        } else if (coffeeBeansNow < latteCoffeeBeans) {
                            System.out.println("Sorry, not enough Coffee Beans!");
                        } else if (moneyNow <= 0) {
                            System.out.println("Sorry, not enough money for change!");
                        } else if (disposableCupsNow <= 0) {
                            System.out.println("Sorry, not enough disposable cups!");
                        } else {
                            System.out.println("I have enough resources, making you a coffee!");
                        }

                        if (waterNow > latteWater && coffeeBeansNow > latteCoffeeBeans && milkNow > latteMilk) {
                            waterNow -= latteWater;
                            coffeeBeansNow -= latteCoffeeBeans;
                            milkNow -= latteMilk;
                            moneyNow += latteMoney;
                            disposableCupsNow -= 1;
                        }

                    } else if (orderCoffee.equals(cappuccinoNum)) {

                        if (waterNow < cappuccinoWater) {
                            System.out.println("Sorry, not enough water!");
                        } else if (coffeeBeansNow < cappuccinoCoffeeBeans) {
                            System.out.println("Sorry, not enough Coffee Beans!");
                        } else if (moneyNow <= 0) {
                            System.out.println("Sorry, not enough money for change!");
                        } else if (disposableCupsNow <= 0) {
                            System.out.println("Sorry, not enough disposable cups!");
                        } else {
                            System.out.println("I have enough resources, making you a coffee!");
                        }

                        if (waterNow > cappuccinoWater && coffeeBeansNow > cappuccinoCoffeeBeans && milkNow > cappuccinoMilk) {
                            waterNow -= cappuccinoWater;
                            coffeeBeansNow -= cappuccinoCoffeeBeans;
                            milkNow -= cappuccinoMilk;
                            moneyNow += cappuccinoMoney;
                            disposableCupsNow -= 1;
                        }


                    } else if (orderCoffee.equals(bck)) {
                        continue;
                    }


                    break;
                case "fill":

                    System.out.print("Write how many ml of water do you want to add: ");
                    int fillWater = sc.nextInt();
                    waterNow += fillWater;

                    System.out.print("Write how many ml of milk do you want to add: ");
                    int fillMilk = sc.nextInt();
                    milkNow += fillMilk;

                    System.out.print("Write how many grams of coffee beans do you want to add: ");
                    int fillCoffeeBeans = sc.nextInt();
                    coffeeBeansNow += fillCoffeeBeans;

                    System.out.print("Write how many disposable cups of coffee do you want to add: ");
                    int filldisposableCups = sc.nextInt();
                    disposableCupsNow += filldisposableCups;

                    break;
                case "take":

                    System.out.println("I give you: " + moneyNow);
                    moneyNow -= moneyNow;

                    break;
                case "remaining":

                    System.out.println();
                    System.out.println("The coffee machine has: ");
                    System.out.println(waterNow + " ml of water");
                    System.out.println(milkNow + " ml of milk");
                    System.out.println(coffeeBeansNow + " gr of coffee beans");
                    System.out.println(disposableCupsNow + " disposable cups");
                    System.out.println("$" + moneyNow + " of money");

                    break;
                case "exit":
                    return;
            }

        } while (true);

    }
}

