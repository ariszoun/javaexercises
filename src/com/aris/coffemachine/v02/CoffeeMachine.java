package com.aris.coffemachine.v02;

import java.util.Scanner;

public class CoffeeMachine {  //MAIN

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        CoffeeMachineConst coffeeMachine = new CoffeeMachineConst(400, 540, 120, 9, 550);
        coffeeMachine.start();

        while (coffeeMachine.getState() != CurrentState.SHUTDOWN) {
            coffeeMachine.processInput(scanner.next());
        }

    }

}
