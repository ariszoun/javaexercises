package com.aris.coffemachine.v02;

public enum CurrentState {

    READY,
    SHUTDOWN,
    WATER_NOW,
    MILK_NOW,
    COFFEEBEANS_NOW,
    DISPOSABLECUPS_NOW,
    BUY_CHOICE

}
