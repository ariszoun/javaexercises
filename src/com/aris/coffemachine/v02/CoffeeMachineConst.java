package com.aris.coffemachine.v02;

public class CoffeeMachineConst {

    private int water;
    private int milk;
    private int coffeeBeans;
    private int disposableCups;
    private int money;
    private String input;
    private CurrentState state = CurrentState.READY;

    CoffeeMachineConst(int water, int milk, int coffeeBeans, int disposableCups, int money) {
        this.water = water;
        this.milk = milk;
        this.coffeeBeans = coffeeBeans;
        this.disposableCups = disposableCups;
        this.money = money;

    }

    CurrentState getState() {
        return this.state;
    }


    private void ready(){
        this.state = CurrentState.READY;
        System.out.println();
        System.out.println("Write action: (buy, fill, take, remaining, exit)");
    }


    void start() {
        ready();
    }



    void stop() {
        this.state = CurrentState.SHUTDOWN;
    }



    void processInput(String input) {
        this.input = input;

        switch(this.state) {
            case READY:
                processReadyCommand();
                break;
            case WATER_NOW:
            case MILK_NOW:
            case COFFEEBEANS_NOW:
            case DISPOSABLECUPS_NOW:
                fill();
                break;
            case BUY_CHOICE:
                buy();
                break;
            default:
                System.out.println("Unknown Input State");
                ready();
                break;
        }
    }



    void processReadyCommand() {
        System.out.println();
        switch (input) {
            case "buy":
                buy();
                break;
            case "fill":
                fill();
                break;
            case "take":
                take();
                break;
            case "remaining":
                printRemaining();
                break;
            case "exit":
                stop();
                break;
            default:
                System.out.println("Unknown Command");
                break;
        }
    }



    private void buy() {
        switch (this.state) {
            case READY:
                System.out.print("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu: ");
                this.state = CurrentState.BUY_CHOICE;
                break;
            case BUY_CHOICE:
                boolean enough = isEnough(this.input);

                switch (this.input) {
                    case "1": //espresso
                        if (enough) {
                            this.water -= 250;
                            this.coffeeBeans -= 16;
                            this.disposableCups -= 1;
                            this.money += 4;
                        }
                        break;
                    case "2": //latte
                        if (enough) {
                            this.water -= 350;
                            this.milk -= 75;
                            this.coffeeBeans -= 20;
                            this.disposableCups -= 1;
                            this.money += 7;
                        }
                        break;
                    case "3": // cappuccino
                        if (enough) {
                            this.water -= 200;
                            this.milk -= 100;
                            this.coffeeBeans -= 12;
                            this.disposableCups -= 1;
                            this.money += 6;
                        }
                        break;
                    case "back":
                        break;
                    default:
                        System.out.println("Unknown buy command");
                        break;
                }
                ready();
                break;
            default:
                System.out.println("Unknown buy State");
                ready();
                break;

        }
    }



    private void fill() {
        switch (this.state) {
            case READY:
                System.out.print("Write how many ml of water do you want to add: ");
                this.state = CurrentState.WATER_NOW;
                break;
            case WATER_NOW:
                this.water += Integer.parseInt(this.input);
                System.out.print("Write how many ml of milk do you want to add: ");
                this.state = CurrentState.MILK_NOW;
                break;
            case MILK_NOW:
                this.milk += Integer.parseInt(this.input);
                System.out.print("Write how many grams of coffee beans do you want to add: ");
                this.state = CurrentState.COFFEEBEANS_NOW;
                break;
            case COFFEEBEANS_NOW:
                this.coffeeBeans += Integer.parseInt(this.input);
                System.out.print("Write how many disposable cups of coffee do you want to add: ");
                this.state = CurrentState.DISPOSABLECUPS_NOW;
                break;
            case DISPOSABLECUPS_NOW:
                this.disposableCups += Integer.parseInt(this.input);
                ready();
                break;
            default:
                System.out.println("Unknown fill state");
                ready();
                break;
        }
    }



    private boolean isEnough(String type) {
        boolean enough = false;

        int waterLimit;
        int milkLimit;
        int beansLimit;

        switch (type) {
            case "1": // espresso
                waterLimit = 250;
                milkLimit = 0;
                beansLimit = 16;
                break;
            case "2": // latte
                waterLimit = 350;
                milkLimit = 75;
                beansLimit = 20;
                break;
            case "3": // cappuccino
                waterLimit = 200;
                milkLimit = 100;
                beansLimit = 12;
                break;
            default:
                return false;
        }

        if (this.water < waterLimit) {
            System.out.println("Sorry, not enough water!");
        } else if (this.milk < milkLimit) {
            System.out.println("Sorry, not enough milk!");
        } else if (this.coffeeBeans < beansLimit) {
            System.out.println("Sorry, not enough coffee beans!");
        } else if (this.disposableCups < 1) {
            System.out.println("Sorry, not enough disposable cups!");
        } else {
            enough = true;
            System.out.println("I have enough resources, making you a coffee!");
        }

        return enough;
    }



    private void take() {
        System.out.println("I gave you $" + this.money);
        this.money = 0;
        ready();
    }

    private void printRemaining() {
        System.out.println("The coffee machine has:");
        System.out.println(this.water + " of water");
        System.out.println(this.milk + " of milk");
        System.out.println(this.coffeeBeans + " of coffee beans");
        System.out.println(this.disposableCups + " of disposable cups");
        System.out.println("$" + this.money + " of money");
        ready();
    }


}