package com.aris.oop03orders;

public class BaseHamburger {

    private String name;
    private String breadRollType;
    private String meat;
    private int price;


    public BaseHamburger(String name, String meat, String breadRollType, int price) {
        this.name = name;
        this.meat = meat;
        this.price = price;
        this.breadRollType = breadRollType;
    }


    public void addTomato(int price) {
        this.price += price;
    }
    public void addLettuce(int price) {
        this.price += price;
    }
    public void addOnion(int price) {
        this.price += price;
    }
    public void addSauce(int price) {
        this.price += price;
    }




    public String getName() {
        return name;
    }
    public String getMeat() {
        return meat;
    }
    public int getPrice() {
        return price;
    }
    public String getBreadRollType() {
        return breadRollType;
    }



    public void printOrder(){
        System.out.println(getName() + " burger");
        System.out.println("Roll type: " + getBreadRollType());
        System.out.println("Meat: " + getMeat());
        System.out.println("Final price: " + getPrice());

    }
}
