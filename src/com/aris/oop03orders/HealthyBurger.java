package com.aris.oop03orders;

public class HealthyBurger extends BaseHamburger {

    private int price;

    public HealthyBurger(int price) {
        super("Healthy", "Chicken", "Brown Rye", 6);
        this.price = price;
    }

    public void addPickles(int price) {
        this.price += price;
    }
    public void addSpinach(int price) {
        this.price += price;
    }





}
