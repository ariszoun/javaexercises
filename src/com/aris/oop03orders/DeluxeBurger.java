package com.aris.oop03orders;

public class DeluxeBurger extends BaseHamburger {

    private String chips;
    private String drinks;

    public DeluxeBurger(String chips, String drinks) {
        super("Deluxe", "Angus Beef", "Premium wheat", 10);
        this.chips = chips;
        this.drinks = drinks;
    }

    public String getChips() {
        return chips;
    }

    public String getDrinks() {
        return drinks;
    }

    @Override
    public void printOrder() {
        System.out.println("Chips: " + getChips());
        System.out.println("Drink: " + getDrinks());
        super.printOrder();

    }
}
