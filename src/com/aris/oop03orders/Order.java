package com.aris.oop03orders;

import java.util.Scanner;

public class Order {

    Scanner sc = new Scanner(System.in);
    BaseHamburger BH1;
    HealthyBurger HB1;
    DeluxeBurger DX1;

    int counter = 0;
    public void order() {

        System.out.println("WELCOME TO ARIS BURGERS!!"); // Base Burger costs 5euro and has only " + BH1.getMeat() + " and " + BH1.getBreadRollType());

        System.out.println("CHOOSE BURGER: BASE - HEALTHY - DELUXE");
        String selection = sc.next();
        switch (selection) {

            case "BASE":
                BH1 = new BaseHamburger("Simple", "Beef", "Corn Bread", 5);
                System.out.println("Want additions? (Y/N)");
                String choice = sc.next();
                if (choice.equals("N")) {
                    BH1.printOrder();
                } else {
                    while (true) {
                        System.out.println("Enter preferable additions: (tomato, lettuce, onion, sauce)");
                        String answer = sc.next();
                        counter++;

                        if (counter <= 4) {
                            if (answer.equals("tomato")) {
                                BH1.addTomato(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            } else if (answer.equals("lettuce")) {
                                BH1.addLettuce(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            } else if (answer.equals("onion")) {
                                BH1.addOnion(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            } else if (answer.equals("sauce")) {
                                BH1.addSauce(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            }
                        }else
                            break;
                    }
                    BH1.printOrder();
                }
                break;

            case "HEALTHY":
                HB1 = new HealthyBurger(6);
                System.out.println("Want additions? (Y/N)");
                choice = sc.next();
                if (choice.equals("N")) {
                    HB1.printOrder();
                } else {
                    while (true) {
                        System.out.println("Enter preferable additions: (tomato, lettuce, onion, sauce, pickles, spinach)");
                        String answer = sc.next();
                        counter++;

                        if (counter <= 6) {
                            if (answer.equals("tomato")) {
                                HB1.addTomato(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            } else if (answer.equals("lettuce")) {
                                HB1.addLettuce(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            } else if (answer.equals("onion")) {
                                HB1.addOnion(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            } else if (answer.equals("sauce")) {
                                HB1.addSauce(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            } else if (answer.equals("pickles")) {
                                HB1.addPickles(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            } else if (answer.equals("spinach")) {
                                HB1.addSpinach(1);
                                HB1.addPickles(1);
                                System.out.println("Break? (Y/N)");
                                String answer2 = sc.next();
                                if (answer2.equals("Y")) {
                                    break;
                                }
                            }
                        }else
                            break;
                    }
                    HB1.printOrder();
                }
                break;

            case "DELUXE":
                DX1 = new DeluxeBurger("Country", "Cola");
                System.out.println("NO ADDITIONS ALLOWED IN THIS SELECTION");
                DX1.printOrder();
        }



    }

}
