package com.aris.chessframeswing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class ChessFrame extends JFrame {

    private BoardPanel panel;

    public ChessFrame() {

        panel = new BoardPanel();
        this.setContentPane(panel);

        MouseMoveListener listener = new MouseMoveListener();
        panel.addMouseMotionListener(listener);

        this.setVisible(true);
        this.setSize(600, 600); //pixel
        this.setTitle("Chess Board");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    //Inner Class
    class BoardPanel extends JPanel {

        private static final int ROWS = 8; //σταθερά private γιατί θα χρησιμοποιηθεί μόνο στον κώδικα της κλάσης αυτής
        private static final int COLUMNS = 8;
        private int xCoord = 50;
        private int yCoord = 50;

        public void setXCoord(int x) {
            xCoord = x;
        }

        public void setYCoord(int y) {
            yCoord = y;
        }


        public void paintComponent(Graphics g) {

            int sqSize = this.getHeight() / ROWS;
            for(int i=0; i<ROWS; i++) {
                for(int j=0; j<COLUMNS; j++) {
                    int x = j * sqSize;
                    int y = i * sqSize;
                    g.setColor(Color.WHITE);
                    g.fillRect(x, y, sqSize, sqSize);
                    if((i + j) % 2 == 0) {
                        g.setColor(Color.BLACK);
                        g.fillRect(x, y, sqSize, sqSize);
                    }
                }

                g.setColor(Color.GRAY);
                g.fillOval(xCoord, yCoord, 40, 40); //oval (circle) dimensions

            }
        }
    }

    class MouseMoveListener implements MouseMotionListener {

        @Override
        public void mouseDragged(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();

            panel.setXCoord(x-20); //sets click on center of the oval (circle)
            panel.setYCoord(y-20); //sets click on center of the oval (circle)
            panel.repaint();

        }

        @Override
        public void mouseMoved(MouseEvent e) { }

    }
}
