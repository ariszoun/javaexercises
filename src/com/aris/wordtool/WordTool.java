package com.aris.wordtool;

public class WordTool {

    WordTool() {}

    public static void main(String[] args) {
        WordTool wt = new WordTool();
        String text = "Bad emotions are relieved by the sea.";

        System.out.println("The word count is: " + wt.wordCount(text));
        System.out.println("\nTotal chars with spaces are: " + wt.charCount(text, true));
        System.out.println("\nTotal chars without spaces are: " + wt.charCount(text, false));
        System.out.println("\n\nTotal amount of chars: " + wt.specificCharCount(text, 'a'));
    }

    public int wordCount (String s) {
        int count = 0;
        if(!(s == null || s.isEmpty())) {
            String[] w = s.split("\\s+");
            count = w.length;
        }
        return count;
    }

    public int charCount(String s, boolean withSpaces) {
        int count = 0;
        if(!(s == null || s.isEmpty())) {
            if (withSpaces) {
                count = s.length();
            } else {
                count = s.replace(" ", "").length();
            }
        }
        return count;
    }

    public int specificCharCount(String s, char c) {
        int count = 0;
        if(!(s == null || s.isEmpty())) {
            //String w = s.replace(" ", "");
            char[] b = new char[s.length()];
            for(int i=0; i<s.length(); i++){
                b[i] += s.charAt(i);
            }

            for(char h : b) {
                if(h == c) {
                    count++;
                }
            }
        }
        return count;
    }

}
