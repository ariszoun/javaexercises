package com.aris.containersswing;

public class Bulk extends Container {

    private int weight;

    public Bulk(String aCode, String aDestination, int weight) {
        super(aCode, aDestination);
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public double getCharge() {
        return 10 * weight;
    }

}
