package com.aris.containersswing;

public class Refrigerator extends Container {

    private double power;

    public Refrigerator(String aCode, String aDestination, double power) {
        super(aCode, aDestination);
        this.power = power;
    }

    public double getCharge() {
        return 2000 * power;
    }
}
