package com.aris.containersswing;

public abstract class Container {

    private String code;
    private String destination;

    public Container(String aCode, String aDestination) {
        this.code = aCode;
        this.destination = aDestination;
    }

    public abstract double getCharge();

}
