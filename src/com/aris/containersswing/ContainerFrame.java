package com.aris.containersswing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ContainerFrame extends JFrame {

    private JTextField codeField;
    private JTextField destinationField;
    private JTextField weightField;
    private JTextField powerField;
    private JButton createBulkButton;
    private JButton createRefrigeratorButton;
    private JList shipList;
    private JPanel containerPanel;
    private JPanel mainPanel; //Panel that includes all UI components
    private ArrayList<Ship> ships;
//    private JTextField containersField;



    public ContainerFrame(ArrayList<Ship> someShips) {

        ships = someShips;

        codeField = new JTextField("Code");
        destinationField = new JTextField("Destination");
        weightField = new JTextField("Weight");
        powerField = new JTextField("Power");
        createBulkButton = new JButton("Create Bulk");
        createRefrigeratorButton = new JButton("Create Refrigerator");
        shipList = new JList();
        containerPanel = new JPanel();
        mainPanel = new JPanel();
//        containersField = new JTextField("Total Charges");

        //Layout manager for containerPanel
        GridLayout grid = new GridLayout(3, 2);
        containerPanel.setLayout(grid);

        containerPanel.add(codeField);
        containerPanel.add(destinationField);
        containerPanel.add(weightField);
        containerPanel.add(powerField);
        containerPanel.add(createBulkButton);
        containerPanel.add(createRefrigeratorButton);
//        containerPanel.add(containersField);

        //Layout manager for mainPanel
        BorderLayout border = new BorderLayout();
        mainPanel.setLayout(border);

        mainPanel.add(shipList, BorderLayout.NORTH);
        mainPanel.add(containerPanel, BorderLayout.CENTER);
//        mainPanel.add(containersField, BorderLayout.SOUTH);


        //Ship List
        DefaultListModel model = new DefaultListModel();

        for(Ship ship: ships) {
            model.addElement(ship.getName());
        }

        shipList.setModel(model);

        this.setContentPane(mainPanel);

        ButtonListener listener = new ButtonListener();
        createBulkButton.addActionListener(listener);
        createRefrigeratorButton.addActionListener(listener);

        this.setVisible(true);
        this.setSize(400, 400);
        this.setTitle("Container Frame");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            String code = codeField.getText();
            String destination = destinationField.getText();

            //getSelectedValue() returns the highlighted element from user
            String selectedShipName = (String) shipList.getSelectedValue();

            Ship selectedShip = null;
            for(Ship ship: ships) {
                if(ship.getName().equals(selectedShipName)) {
                    selectedShip = ship;
                }
            }

            if(e.getSource() == createBulkButton) {

                String weightText = weightField.getText(); //getText is a String, I need integer for weight
                int weight = Integer.parseInt(weightText);
                Bulk newContainer = new Bulk(code, destination, weight);
                selectedShip.addContainer(newContainer);
            } else {

                String powerText = powerField.getText();
                double power = Double.parseDouble(powerText);
                Refrigerator newContainer = new Refrigerator(code, destination, power);
                selectedShip.addContainer(newContainer);

            }


//            double charge = selectedShip.getTotalCharge();
//            String chargeText = String.valueOf(charge);
//            containersField.setText(chargeText);

//            System.out.println("Selected ship charge: " + selectedShip.getTotalCharge());
        }
    }

}
