package com.aris.containersswing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ChargeCalculator extends JFrame {


    private JButton claculateChargeButton;
    private JButton openContainerFrameButton;
    private JTextField showChargesField;
    private JPanel panel;
    private JList shipList;
    private ArrayList<Ship> ships;


    public ChargeCalculator(ArrayList<Ship> someShips) {

        ships = someShips;

        claculateChargeButton = new JButton("Calculate Charge");
        openContainerFrameButton = new JButton("Container Frame");
        shipList = new JList();
        panel = new JPanel();
        showChargesField = new JTextField("Total Charges");


        GridLayout grid = new GridLayout(2,1);
        panel.setLayout(grid);
        panel.add(openContainerFrameButton);
        panel.add(shipList);
        panel.add(claculateChargeButton);
        panel.add(showChargesField);


        //Ship List
        DefaultListModel model = new DefaultListModel();

        for(Ship ship: ships) {
            model.addElement(ship.getName());
        }

        shipList.setModel(model);


        this.setContentPane(panel);

        //Listeners
        ButtonListener listener = new ButtonListener();
        claculateChargeButton.addActionListener(listener);

        openContainerFrameButtonListener listener2 = new openContainerFrameButtonListener();
        openContainerFrameButton.addActionListener(listener2);

        this.setVisible(true);
        this.setSize(600, 200);
        this.setTitle("Charge Calculator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    class ButtonListener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {
            //getSelectedValue() returns the highlighted element from user
            String selectedShipName = (String) shipList.getSelectedValue();
            Ship selectedShip = null;

            for(Ship ship: ships) {
                if(ship.getName().equals(selectedShipName)) {
                    selectedShip = ship;
                }
            }

            if(e.getSource() == claculateChargeButton) {
                double charge = selectedShip.getTotalCharge();
                String chargeText = String.valueOf(charge);
                showChargesField.setText(chargeText);
            }
        }
    }

    class openContainerFrameButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            new ContainerFrame(ships);
        }
    }
}
