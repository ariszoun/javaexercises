package com.aris.interfaces.saveable;

import java.util.ArrayList;
import java.util.Scanner;

public class InterfacesMain {
    public static void main(String[] args) {

        Player aris = new Player("Aris", 100, 120);
        System.out.println(aris.toString());
        saveObject(aris);
        aris.setHitPoints(90);
        System.out.println(aris.toString());
        aris.setWeapon("Swornbreaker");
        saveObject(aris);
        //loadObject(aris);
        System.out.println(aris.toString());

        ISaveable harpy = new Monster("Harpy", 150, 100);

        //cast to access specific methods or change type of harpy into Monster instead of ISaveable
        System.out.println("Harpy's strength: " + ((Monster) harpy).getStrength());

        System.out.println(harpy.toString());
        saveObject(harpy);

    }

    public static ArrayList<String> readValues() {
        ArrayList<String> values = new ArrayList<String>();

        Scanner scanner = new Scanner(System.in);
        boolean quit = false;
        int index = 0;
        System.out.println("Choose\n" +
                "1 to enter a string\n" +
                "0 to quit");

        while (!quit) {
            System.out.print("Choose an option: ");
            int choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 0:
                    quit = true;
                    break;
                case 1:
                    System.out.print("Enter a string: ");
                    String stringInput = scanner.nextLine();
                    values.add(index, stringInput);
                    index++;
                    break;
            }
        }
        return values;
    }

    public static void saveObject(ISaveable objectToSave) {
        for(int i=0; i<objectToSave.write().size(); i++) {
            System.out.println("Saving " + objectToSave.write().get(i) + " into storage...");
        }
    }

    public static void loadObject(ISaveable objectToLoad) {
        ArrayList<String> values = readValues();
        objectToLoad.read(values);
    }
}
