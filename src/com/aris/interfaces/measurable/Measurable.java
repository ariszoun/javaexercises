package com.aris.interfaces.measurable;

public interface Measurable {

    double getMeasure();

}
