package com.aris.interfaces.measurable;/*Κλάση ανάλυσης και εξαγωγής στατιστικών στοιχείων*/

public class DataSet {

    private int counter; //μετρητής των πραγμάτων που έχουμε καταχωρήσειως τώρα σε αυτη
    private double sum; //το άθροισμα των υπολοιπων των τραπεζικων λογαριασμων που έχουμε καταχωρήσει
    private Measurable max;
    private Measurable min;


    //μη απαραίτητος κατασκευαστης. τον χρησιμοποιω για init στις τιμες προαιρετικα
    public DataSet() {
        this.counter = 0;
        this.sum = 0;
        this.max = null;
        this.min = null;
    }

    //θαλω να καταχωρω άρα:
    public void add(Measurable item) {
        if(counter == 0) {
            max = item;
            min = item;
        } else if (item.getMeasure() > max.getMeasure()) {
            max = item;
        } else if (item.getMeasure() < min.getMeasure()) {
            min = item;
        }

        counter++;
        sum += item.getMeasure();
    }

    //επιστρεφει το μεσο ορο των υπολοιπων των λογαριασμων
    public double calcAverage() {
        if(counter == 0) {
            return 0;
        } else {
            return sum / counter;
        }
    }

    //ο επιστερφομενος τυπος ειναι bank account.
    public Measurable getMax() {
        return max;
    }
    //ο επιστερφομενος τυπος ειναι bank account.
    public Measurable getMin() {
        return min;
    }

}
