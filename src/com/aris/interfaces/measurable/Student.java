package com.aris.interfaces.measurable;/* Θα χρησιμοποιήσω το σκαρί της κλάσης DataSet για φοιτητες*/

public class Student implements Measurable {

    private double GPA;

    public Student(double GPA) {
        this.GPA = GPA;
    }

    public double getMeasure() {
        return GPA;
    }
}
