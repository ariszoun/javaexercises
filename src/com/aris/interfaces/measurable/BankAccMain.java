package com.aris.interfaces.measurable;

public class BankAccMain {

    public static void main(String[] args) {

        DataSet ds = new DataSet();

        BankAccount BA1 = new BankAccount(3000);
        BankAccount BA2 = new BankAccount(4000);
        BankAccount BA3 = new BankAccount(1000);

        Student S1 = new Student(8.23);
        Student S2 = new Student(9.11);
        Student S3 = new Student(7.88);

        ds.add(BA1);
        ds.add(BA2);
        ds.add(BA3);

        System.out.println("Average balances of all Accounts: " + ds.calcAverage());
        System.out.println("Account with Max Balance: " + ds.getMax().getMeasure());
        System.out.println("Account with Min Balance: " + ds.getMin().getMeasure());


    }

}
