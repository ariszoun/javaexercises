package com.aris.abstractclasses;

public class AbstractClassesMain {

    public static void main(String[] args) {

        MyLinkedList list = new MyLinkedList(null);
        list.traverse(list.getRoot());


        String stringData = "Darwin Brisbane Perth Melbourne Canberra Adelaide Sydney Canberra";
        String stringData2 = "6 4 9 6 0 9 6 2 3 5 2 1 5 3 7 9 0 8 8 6 5 3 3 9 1 1";

        String[] data = stringData2.split(" ");
        for (String s : data) {
            list.addItem(new Node(s));
        }


        list.traverse(list.getRoot());
        list.removeItem(new Node("3"));
        list.traverse(list.getRoot());

        list.removeItem(new Node("1"));
        list.removeItem(new Node("0"));
        list.removeItem(new Node("5"));
        list.traverse(list.getRoot());


    }

}
